<!DOCTYPE html>
<html>
<head>
    <title>Calculate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style> 
    body {
        background: powderblue;;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        flex-direction: column;
    }

    * {
        font-family: sans-serif;
        box-sizing: border-box;
    }

    form {
        width: 700px;
        border: 2px solid #ccc;
        padding: 30px;
        background: #fff;
        border-radius: 15px;
    }


    input {
        display: block;
        border: 2px solid #ccc;
        width: 95%;
        padding: 10px;
        margin: 10px auto;
        border-radius: 5px;
    }


    .error {
        background: #F2DEDE;
        color: #A94442;
        padding: 10px;
        width: 95%;
        border-radius: 5px;
        margin: 20px auto;
    }

    h3 {
        text-align: left;
        color: black;
    }

</style>
<?php
// declaration of object
$Voltage = $_POST['Voltage'];
$Current = $_POST['Current'];
$CurrentRate = $_POST['CurrentRate'];
$Hours = $_POST['Hours'];

//calculate formula
$Power = ($Voltage * $Current);
$Energy = ($Power * $Hours) / 1000;
$Total = $Energy * ($CurrentRate / 100);
?>

<body >
    <form action="" method="post" id="quiz-form">
        <h1><font>
            Calculate
        </font><br></h1>
        <h3>Voltage :</h3><input type="text" name="Voltage" id="Voltage" required="required" value="<?php echo $Voltage; ?>">
        <h3>Current:</h3><input type="text" name="Current" id="Current" required="required" value="<?php echo $Current; ?>">
        <h3>Current Rate :</h3><input type="text" name="CurrentRate" id="CurrentRate" required="required" value="<?php echo $CurrentRate; ?>">
        <h3>Hour:</h3><input type="text" name="Hours" id="Hours" required="required" value="<?php echo $Hours; ?>">

        <input type="submit" value="Submit">
        <table style="width:100%">
            <tr>
                <td>Poweh(Wh)</td>
                <td>Energy(kWH)</td>
                <td>Total</td>
            </tr>
            <tr>
                <td><input readonly="readonly" name="CalculatorResult" value="<?php echo $Power; ?>"></td>
                <td><input readonly="readonly" name="CalculatorResult" value="<?php echo $Energy; ?>"></td>
                <td><input readonly="readonly" name="CalculatorResult" value="<?php echo $Total; ?>"></td>
            </tr>
        </table>
    </form>
</body>

</html>